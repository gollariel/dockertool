package utils

import (
	"testing"

	"bytes"
	"github.com/magiconair/properties/assert"
	"strings"
)

func TestConcat(t *testing.T) {
	testcases := map[string]struct {
		inStrs []string
		outStr string
	}{
		"inStrs:empty":        {inStrs: []string{}, outStr: ""},
		"inStrs:nil":          {inStrs: nil, outStr: ""},
		"inStrs:non_empty":    {inStrs: []string{"hello", " ", "world"}, outStr: "hello world"},
		"inStrs:MORE_THAN_32": {inStrs: []string{"hello", " ", "world", " ", "hello", " ", "world", " ", "hello", " ", "world", " ", "hello", " ", "world"}, outStr: "hello world hello world hello world hello world"},
	}

	for k, c := range testcases {
		t.Run(k, func(t *testing.T) {
			str := Concat(c.inStrs...)
			assert.Equal(t, str, c.outStr)
		})
	}
}

/*
BenchmarkConcat-4                       20000000                70.6 ns/op
BenchmarkConcatWithBytesBuffer-4        10000000               167 ns/op
BenchmarkConcatWithRawStringsJoin-4     20000000                96.9 ns/op
*/

var (
	strsForConcat      = []string{"hello", " ", "world", " ", "from", " ", "Golang"}
	globalResForConcat string
)

func BenchmarkConcat(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = Concat(strsForConcat...)
	}
	globalResForConcat = localRes
}

func BenchmarkConcatWithBytesBuffer(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		buff := bytes.NewBuffer(nil)
		for _, s := range strsForConcat {
			_, _ = buff.WriteString(s)
		}
		localRes = buff.String()
	}
	globalResForConcat = localRes
}

func BenchmarkConcatWithRawStringsJoin(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = strings.Join(strsForConcat, "")
	}
	globalResForConcat = localRes
}
