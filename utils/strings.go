package utils

// Concat concatnates given strings
func Concat(strs ...string) string {
	if len(strs) == 0 {
		return ""
	}

	var cnt int
	for _, str := range strs {
		cnt += len(str)
	}

	if cnt <= 32 {
		b := []byte(strs[0])
		for i := 1; i < len(strs); i++ {
			b = append(b, strs[i]...)
		}

		return string(b)
	}

	b := make([]byte, cnt)
	var i int
	for _, str := range strs {
		i += copy(b[i:], str[:])
	}
	return string(b)
}
