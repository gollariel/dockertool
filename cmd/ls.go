// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"os"

	"bitbucket.org/gollariel/dockertool/api"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

// lsCmd represents the ls command
var lsCmd = &cobra.Command{
	Use:   "ls [container_id] [path]",
	Short: "Display the list of files inside a container",
	Long: `This command will display the list of files inside a container, starting from path. For example docktool ls container
/ would display all files in that container.`,
	Example: "  dockertool ls am_app_1 ./",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 2 {
			color.Red("Not enough arguments")
			err := cmd.Help()
			if err != nil {
				log.Fatalln(err)
			}
			os.Exit(1)
		}
		err := api.ListFiles(dockerTool, args[0], args[1])
		if err != nil {
			log.Fatalf("Error ls for container %s and path %s \n", args[0], args[1])
		}
	},
}

func init() {
	rootCmd.AddCommand(lsCmd)
}
