// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"os"

	"bitbucket.org/gollariel/dockertool/api"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

// findcontainerCmd represents the findcontainer command
var findcontainerCmd = &cobra.Command{
	Use:   "findcontainer [search_string]",
	Short: "Search container by substring",
	Long: `This command will display all containers that match given name. It should match substrings too, for example docktool
findcontainer front would display containers with names like frontend , frontier , tofront , etc.`,
	Example: "  docketool findcontainer am",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			color.Red("Not enough arguments")
			err := cmd.Help()
			if err != nil {
				log.Fatalln(err)
			}
			os.Exit(1)
		}
		err := api.FindContainer(dockerTool, args[0])
		if err != nil {
			log.Fatalf("Error findcontainer for value: %s\n", args[0])
		}
	},
}

func init() {
	rootCmd.AddCommand(findcontainerCmd)
}
