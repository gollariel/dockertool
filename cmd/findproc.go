// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"os"

	"bitbucket.org/gollariel/dockertool/api"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

// findprocCmd represents the findproc command
var findprocCmd = &cobra.Command{
	Use:   "findproc [search_string]",
	Short: "Search process in container by substring",
	Long: `This command will display the container(s) that are running a process with the given name. The output must include the
container Id, the container name and the command. It should match substrings too, for example docktool
findproc sh will display all container that are running bash , sh , zsh , etc.`,
	Example: "  dockertool findproc sh",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			color.Red("Not enough arguments")
			err := cmd.Help()
			if err != nil {
				log.Fatalln(err)
			}
			os.Exit(1)
		}
		err := api.FindProcesses(dockerTool, args[0])
		if err != nil {
			log.Fatalf("Error findproc for value: %s\n", args[0])
		}
	},
}

func init() {
	rootCmd.AddCommand(findprocCmd)
}
