# Dockertool

## docktool findcontainer [name]
This command will display all containers that match given name. It should match substrings too, for example docktool
findcontainer front would display containers with names like frontend , frontier , tofront , etc.

## docktool findproc [proc_name]
This command will display the container(s) that are running a process with the given name. The output must include the
container Id, the container name and the command. It should match substrings too, for example docktool
findproc sh will display all container that are running bash , sh , zsh , etc.

## docktool ls [container_id] [path]
This command will display the list of files inside a container, starting from path. For example docktool ls container
/ would display all files in that container.

# Installation

```
make dockertool
make install
```

# Run via docker

```
make docker_app
docker run -it --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock dockertool [command]
```

# Build via docker

```
make workspace
docker exec -it dockertool_workspace bash
make dockertool
```

# Test code

```
make test
make bench
```

# Check code

Must be installed all dependencies.

```
make lint
```