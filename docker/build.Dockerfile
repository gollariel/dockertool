FROM golang:1.10-stretch

ENV GOPATH /app/go
RUN mkdir -p ${GOPATH}/src/bitbucket.org/gollariel/dockertool
WORKDIR ${GOPATH}/src/bitbucket.org/gollariel/dockertool

ENTRYPOINT ["tail", "-f", "/dev/null"]