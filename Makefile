ALL: format build

BUILD := `git rev-parse HEAD`
LDFLAGS=-ldflags "-X bitbucket.org/gollariel/dockertool/cmd.version=$(BUILD)"

format:
	gofmt -w=true **/*.go

test:
	go test -race -p 1 ./...

bench:
	go test -bench=. ./...

dockertool:
	go build $(LDFLAGS) -o build/dockertool ./main.go

dockertool_linux:
	GOOS=linux GOARCH=amd64 go build $(LDFLAGS) -o build/dockertool_linux ./main.go

install:
	mkdir -p ${GOPATH}/build/
	cp build/* ${GOPATH}/build/

workspace:
	docker build -f docker/build.Dockerfile -t dockertool_workspace ./
	docker run -d --restart=always --name=dockertool_workspace -v $(pwd):/app/go/src/bitbucket.org/gollariel/dockertool dockertool_workspace

docker_app:
	make dockertool_linux
	docker build -f docker/Dockerfile -t dockertool ./

deps:
	go install github.com/Masterminds/glide
	# Linters
	go install github.com/golang/lint/golint
	go install github.com/alecthomas/gometalinter
	go install github.com/tsenart/deadcode
	go install github.com/opennota/check/cmd/structcheck
	go install github.com/opennota/check/cmd/varcheck
	go install github.com/kisielk/errcheck
	go install github.com/gordonklaus/ineffassign
	go install honnef.co/go/tools/cmd/megacheck

lint: deps
	gometalinter \
		--vendor \
		--skip=vendor \
		--skip=managefb \
		--disable-all \
		--enable=golint \
		--enable=deadcode \
		--enable=structcheck \
		--enable=varcheck \
		--enable=errcheck \
		--enable=ineffassign \
		--enable=vet \
		--enable=megacheck \
		--deadline=3m \
		./...
