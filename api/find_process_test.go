package api

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestFindProcesses(t *testing.T) {
	stdout := &OutputMock{}
	dockerTool, err := GetTestDockerTool(stdout, GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	err = FindProcesses(dockerTool, "test")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, stdout.Result, `+-------------+---------------+------+
| CONTAINERID |     NAMES     | CMD  |
+-------------+---------------+------+
|           1 | redis         | test |
|           2 | test_app_1    | test |
|           3 | mongo mongodb | test |
|           4 | memsql        | test |
|           5 | aerospike     | test |
|           6 | test_app_2    | test |
+-------------+---------------+------+
`)
}
