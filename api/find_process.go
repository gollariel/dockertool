package api

import (
	"strings"

	"github.com/olekukonko/tablewriter"
)

// FindProcesses search processes in all active containers
func FindProcesses(d DockerTool, filter string) error {
	containers, err := d.GetContainers(false, "")
	if err != nil {
		return err
	}

	table := tablewriter.NewWriter(d.GetStdout())
	header := []string{"ContainerID", "Names"}
	showColumns := d.GetConfig().GetStringSlice("commands.findproc.show_columns")
	header = append(header, showColumns...)
	table.SetHeader(header)

	searchColumns := d.GetConfig().GetStringMap("commands.findproc.search_columns")
	for _, container := range containers {
		title, processes, err := d.ContainerTop(container.ID, filter, searchColumns)
		if err != nil {
			return err
		}

		heads := map[int]struct{}{}
		for i, name := range title {
			for _, column := range showColumns {
				if column == name {
					heads[i] = struct{}{}
				}
			}
		}

		for _, process := range processes {
			row := []string{
				container.ID,
				strings.Join(container.Names, " "),
			}

			for i, param := range process {
				if _, ok := heads[i]; ok {
					row = append(row, param)
				}
			}

			table.Append(row)
		}
	}

	table.Render()

	return nil
}
