package api

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestListFiles(t *testing.T) {
	stdout := &OutputMock{}
	dockerTool, err := GetTestDockerTool(stdout, GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	err = ListFiles(dockerTool, "test_app_1", "./")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, stdout.Result, `/opt/aerospike/client/sys/udf/lua:
aerospike.lua
as.lua
stream_ops.lua

/opt/aerospike/client/usr:
udf
`)
}
