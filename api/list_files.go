package api

import (
	"fmt"
)

// ListFiles return all files in container for path
func ListFiles(d DockerTool, container string, path string) error {
	result, err := d.ContainerExec(container, []string{
		"ls",
		d.GetConfig().GetString("commands.ls.flags"),
		path,
	})

	if err != nil {
		return err
	}

	fmt.Fprint(d.GetStdout(), string(result))

	return nil
}
