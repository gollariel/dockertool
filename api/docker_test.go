package api

import (
	"bufio"
	"context"
	"io"
	"net"
	"strings"
	"testing"

	"bitbucket.org/gollariel/dockertool/utils"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/magiconair/properties/assert"
	"github.com/spf13/viper"
)

type ClientMock struct{}

func (c *ClientMock) ContainerList(ctx context.Context, options types.ContainerListOptions) ([]types.Container, error) {
	return []types.Container{
		{
			ID:      "1",
			Names:   []string{"redis"},
			Image:   "redis:latest",
			Command: "tail -f /dev/null",
		},
		{
			ID:      "2",
			Names:   []string{"test_app_1"},
			Image:   "test_app:1.2",
			Command: "tail -f /dev/null",
		},
		{
			ID:      "3",
			Names:   []string{"mongo", "mongodb"},
			Image:   "mongo:latest",
			Command: "tail -f /dev/null",
		},
		{
			ID:      "4",
			Names:   []string{"memsql"},
			Image:   "memsql:latest",
			Command: "tail -f /dev/null",
		},
		{
			ID:      "5",
			Names:   []string{"aerospike"},
			Image:   "aerospike:latest",
			Command: "tail -f /dev/null",
		},
		{
			ID:      "6",
			Names:   []string{"test_app_2"},
			Image:   "test_app:latest",
			Command: "tail -f /dev/null",
		},
	}, nil
}
func (c *ClientMock) ContainerTop(ctx context.Context, containerID string, arguments []string) (container.ContainerTopOKBody, error) {
	return container.ContainerTopOKBody{
		Titles: []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
		Processes: [][]string{
			{"999", "2700", "2575", "0", "may22", "?", "00:12:59", "mongod --bind_ip_all"},
			{"1000", "2701", "2576", "0", "may22", "?", "00:12:59", "tail /dev/null"},
			{"1001", "2702", "2577", "0", "may22", "?", "00:12:59", "bash"},
			{"1002", "2703", "2578", "0", "may22", "?", "00:12:59", "test"},
			{"1002", "2704", "2579", "0", "may22", "?", "00:12:59", "zsh"},
		},
	}, nil
}
func (c *ClientMock) ContainerExecCreate(ctx context.Context, container string, config types.ExecConfig) (types.IDResponse, error) {
	return types.IDResponse{
		ID: "1",
	}, nil
}
func (c *ClientMock) ContainerExecAttach(ctx context.Context, execID string, config types.ExecConfig) (types.HijackedResponse, error) {
	const input = `/opt/aerospike/client/sys/udf/lua:
aerospike.lua
as.lua
stream_ops.lua

/opt/aerospike/client/usr:
udf
`
	return types.HijackedResponse{
		Reader: bufio.NewReader(strings.NewReader(input)),
		Conn:   &net.IPConn{},
	}, nil
}

type OutputMock struct {
	Result string
}

func (o *OutputMock) Write(p []byte) (n int, err error) {
	o.Result = utils.Concat(o.Result, string(p))
	return len(p), nil
}

func GetTestOutput() io.Writer {
	return &OutputMock{}
}

func GetTestConfig(t *testing.T) *viper.Viper {
	config := viper.New()

	config.SetConfigFile("../configs/.dockertool_test.yaml")
	config.AutomaticEnv() // read in environment variables that match

	err := config.ReadInConfig()
	if err != nil {
		t.Fatal(err)
	}

	return config
}

func GetTestDockerTool(stdout io.Writer, viper *viper.Viper) (DockerTool, error) {
	return &DockerToolClient{
		client: &ClientMock{},
		viper:  viper,
		stdout: stdout,
		ctx:    context.Background(),
	}, nil
}

func TestGetConfig(t *testing.T) {
	config := GetTestConfig(t)
	dockerTool, err := GetTestDockerTool(GetTestOutput(), config)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, dockerTool.GetConfig(), config)
}

func TestGetStdout(t *testing.T) {
	stdout := GetTestOutput()
	dockerTool, err := GetTestDockerTool(GetTestOutput(), GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, dockerTool.GetStdout(), stdout)
}

func TestGetContainers(t *testing.T) {
	dockerTool, err := GetTestDockerTool(GetTestOutput(), GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	testCases := map[string]struct {
		filter   string
		expected []types.Container
	}{
		"match_two_containers": {
			filter: "test_app",
			expected: []types.Container{
				{
					ID:      "2",
					Names:   []string{"test_app_1"},
					Image:   "test_app:1.2",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "6",
					Names:   []string{"test_app_2"},
					Image:   "test_app:latest",
					Command: "tail -f /dev/null",
				},
			},
		},
		"no_match_containers": {
			filter:   "a2pp_1",
			expected: []types.Container{},
		},
		"match_alias_containers": {
			filter: "mongodb",
			expected: []types.Container{
				{
					ID:      "3",
					Names:   []string{"mongo", "mongodb"},
					Image:   "mongo:latest",
					Command: "tail -f /dev/null",
				},
			},
		},
		"match_all": {
			filter: "",
			expected: []types.Container{
				{
					ID:      "1",
					Names:   []string{"redis"},
					Image:   "redis:latest",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "2",
					Names:   []string{"test_app_1"},
					Image:   "test_app:1.2",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "3",
					Names:   []string{"mongo", "mongodb"},
					Image:   "mongo:latest",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "4",
					Names:   []string{"memsql"},
					Image:   "memsql:latest",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "5",
					Names:   []string{"aerospike"},
					Image:   "aerospike:latest",
					Command: "tail -f /dev/null",
				},
				{
					ID:      "6",
					Names:   []string{"test_app_2"},
					Image:   "test_app:latest",
					Command: "tail -f /dev/null",
				},
			},
		},
	}

	for name, test := range testCases {
		t.Run(name, func(t *testing.T) {
			result, err := dockerTool.GetContainers(false, test.filter)
			if err != nil {
				t.Fatal(err)
			}

			if len(result) != 0 && len(test.expected) != 0 {
				assert.Equal(t, result, test.expected)
			}
		})
	}
}

func TestContainerTop(t *testing.T) {
	dockerTool, err := GetTestDockerTool(GetTestOutput(), GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	testCases := map[string]struct {
		filter            string
		expectedTitle     []string
		expectedProcesses [][]string
	}{
		"match_cmd": {
			filter:        "sh",
			expectedTitle: []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
			expectedProcesses: [][]string{
				{"1001", "2702", "2577", "0", "may22", "?", "00:12:59", "bash"},
				{"1002", "2704", "2579", "0", "may22", "?", "00:12:59", "zsh"},
			},
		},
		"no_match": {
			filter:            "test2",
			expectedTitle:     []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
			expectedProcesses: [][]string{},
		},
		"match_uid": {
			filter:        "1002",
			expectedTitle: []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
			expectedProcesses: [][]string{
				{"1002", "2703", "2578", "0", "may22", "?", "00:12:59", "test"},
				{"1002", "2704", "2579", "0", "may22", "?", "00:12:59", "zsh"},
			},
		},
		"match_pid": {
			filter:        "2703",
			expectedTitle: []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
			expectedProcesses: [][]string{
				{"1002", "2703", "2578", "0", "may22", "?", "00:12:59", "test"},
			},
		},
		"match_all": {
			filter:        "",
			expectedTitle: []string{"UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"},
			expectedProcesses: [][]string{
				{"999", "2700", "2575", "0", "may22", "?", "00:12:59", "mongod --bind_ip_all"},
				{"1000", "2701", "2576", "0", "may22", "?", "00:12:59", "tail /dev/null"},
				{"1001", "2702", "2577", "0", "may22", "?", "00:12:59", "bash"},
				{"1002", "2703", "2578", "0", "may22", "?", "00:12:59", "test"},
				{"1002", "2704", "2579", "0", "may22", "?", "00:12:59", "zsh"},
			},
		},
	}

	for name, test := range testCases {
		t.Run(name, func(t *testing.T) {
			title, result, err := dockerTool.ContainerTop("", test.filter, dockerTool.GetConfig().GetStringMap("search_columns"))
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, title, test.expectedTitle)

			if len(result) != 0 && len(test.expectedProcesses) != 0 {
				assert.Equal(t, result, test.expectedProcesses)
			}
		})
	}
}

func TestContainerExec(t *testing.T) {
	dockerTool, err := GetTestDockerTool(GetTestOutput(), GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	result, err := dockerTool.ContainerExec("test_app_1", []string{})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, result, `/opt/aerospike/client/sys/udf/lua:
aerospike.lua
as.lua
stream_ops.lua

/opt/aerospike/client/usr:
udf
`)
}
