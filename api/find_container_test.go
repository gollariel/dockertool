package api

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestFindContainer(t *testing.T) {
	stdout := &OutputMock{}
	dockerTool, err := GetTestDockerTool(stdout, GetTestConfig(t))
	if err != nil {
		t.Fatal(err)
	}

	err = FindContainer(dockerTool, "test")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, stdout.Result, `+----+------------+-----------------+-------------------+
| ID |   NAMES    |      IMAGE      |      COMMAND      |
+----+------------+-----------------+-------------------+
|  2 | test_app_1 | test_app:1.2    | tail -f /dev/null |
|  6 | test_app_2 | test_app:latest | tail -f /dev/null |
+----+------------+-----------------+-------------------+
`)
}
