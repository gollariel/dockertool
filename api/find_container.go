package api

import (
	"strings"

	"github.com/olekukonko/tablewriter"
)

// FindContainer search container and print result
func FindContainer(d DockerTool, filter string) error {
	containers, err := d.GetContainers(d.GetConfig().GetBool("commands.findcontainer.all"), filter)

	table := tablewriter.NewWriter(d.GetStdout())
	table.SetHeader([]string{"ID", "Names", "Image", "Command"})
	for _, container := range containers {
		table.Append([]string{
			container.ID,
			strings.Join(container.Names, " "),
			container.Image,
			container.Command,
		})
	}
	table.Render()

	return err
}
