package api

import (
	"io"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
)

// DockerTool interface for dockertool
type DockerTool interface {
	GetContainers(all bool, filter string) ([]types.Container, error)
	ContainerTop(container string, filter string, searchColumns map[string]interface{}) ([]string, [][]string, error)
	ContainerExec(container string, cmd []string) (string, error)
	GetConfig() *viper.Viper
	GetStdout() io.Writer
}

// DockerToolAPIClient interface for docker commands
type DockerToolAPIClient interface {
	ContainerExecAttach(ctx context.Context, execID string, config types.ExecConfig) (types.HijackedResponse, error)
	ContainerExecCreate(ctx context.Context, container string, config types.ExecConfig) (types.IDResponse, error)
	ContainerList(ctx context.Context, options types.ContainerListOptions) ([]types.Container, error)
	ContainerTop(ctx context.Context, container string, arguments []string) (container.ContainerTopOKBody, error)
}
