package api

import (
	"io"
	"io/ioutil"
	"os"
	"strings"
	"syscall"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
)

// DockerToolClient base client
type DockerToolClient struct {
	client DockerToolAPIClient
	viper  *viper.Viper
	ctx    context.Context
	stdout io.Writer
}

// NewDockerTool return DockerTool
func NewDockerTool(viper *viper.Viper) (DockerTool, error) {
	dockerClient, err := client.NewEnvClient()
	if err != nil {
		return nil, err
	}

	return &DockerToolClient{client: dockerClient, viper: viper, ctx: context.Background(), stdout: os.NewFile(uintptr(syscall.Stdout), viper.GetString("stdout"))}, nil
}

// GetConfig return viper instance
func (d *DockerToolClient) GetConfig() *viper.Viper {
	return d.viper
}

// GetStdout return writer
func (d *DockerToolClient) GetStdout() io.Writer {
	return d.stdout
}

// GetContainers return containers by filter
func (d *DockerToolClient) GetContainers(all bool, filter string) ([]types.Container, error) {
	ctx, cancel := context.WithCancel(d.ctx)
	defer cancel()

	containers, err := d.client.ContainerList(ctx, types.ContainerListOptions{
		All: all,
	})

	if filter == "" || err != nil {
		return containers, err
	}

	var result []types.Container
	for _, container := range containers {
		for _, name := range container.Names {
			if strings.Contains(name, filter) {
				result = append(result, container)
				break
			}
		}
	}
	return result, nil
}

// ContainerTop return processes by filter
func (d *DockerToolClient) ContainerTop(container string, filter string, searchColumns map[string]interface{}) ([]string, [][]string, error) {
	ctx, cancel := context.WithCancel(d.ctx)
	defer cancel()

	response, err := d.client.ContainerTop(ctx, container, []string{})
	if filter == "" || err != nil {
		return response.Titles, response.Processes, err
	}

	var result [][]string
	for _, rows := range response.Processes {
		for i, row := range rows {
			title := strings.ToLower(response.Titles[i])
			if _, ok := searchColumns[title]; ok {
				if strings.Contains(row, filter) {
					result = append(result, rows)
					break
				}
			}
		}
	}

	return response.Titles, result, nil
}

// ContainerExec exec command
func (d *DockerToolClient) ContainerExec(container string, cmd []string) (string, error) {
	ctx, cancel := context.WithCancel(d.ctx)
	defer cancel()

	resp, err := d.client.ContainerExecCreate(ctx, container, types.ExecConfig{
		AttachStdin:  true,
		AttachStdout: true,
		AttachStderr: true,
		Cmd:          cmd,
	})
	if err != nil {
		return "", err
	}

	execCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	hijackedResponse, err := d.client.ContainerExecAttach(execCtx, resp.ID, types.ExecConfig{
		Tty: true,
	})
	if err != nil {
		return "", err
	}

	// Don't forget to close the connection:
	defer hijackedResponse.Close()
	s, err := ioutil.ReadAll(hijackedResponse.Reader)

	return string(s), err
}
